Attribute VB_Name = "conexioninternet"
Option Explicit
Public Declare Function InternetGetConnectedState Lib "wininet.dll" (ByRef lpdwFlags As Long, ByVal dwReserved As Long) As Long
Public Const INTERNET_CONNECTION_MODEM As Long = &H1
Public Const INTERNET_CONNECTION_LAN As Long = &H2
Public Const INTERNET_CONNECTION_PROXY As Long = &H4
Public Const INTERNET_CONNECTION_MODEM_BUSY As Long = &H8
Public Const INTERNET_RAS_INSTALLED As Long = &H10
Public Const INTERNET_CONNECTION_OFFLINE As Long = &H20
Public Const INTERNET_CONNECTION_CONFIGURED As Long = &H40
Private Const FLAG_ICC_FORCE_CONNECTION = &H1
Private Declare Function InternetCheckConnection Lib "wininet.dll" Alias "InternetCheckConnectionA" (ByVal lpszUrl As String, ByVal dwFlags As Long, ByVal dwReserved As Long) As Long
Global ConexionOK As Boolean


Public Function GetNetConnectString() As String
Dim dwFlags As Long
Dim msg As String
If InternetGetConnectedState(dwFlags, 0&) Then
If InternetCheckConnection("http://www.microsoft.com/", FLAG_ICC_FORCE_CONNECTION, 0&) = 0 Then
ConexionOK = False
Else
ConexionOK = True
End If
If dwFlags And INTERNET_CONNECTION_CONFIGURED Then
msg = msg & "network" & vbCrLf
End If
If dwFlags And INTERNET_CONNECTION_LAN Then
msg = msg & "LAN"
End If
If dwFlags And INTERNET_CONNECTION_PROXY Then
msg = msg & "proxy server. "
Else: msg = msg & "."
End If
If dwFlags And INTERNET_CONNECTION_MODEM Then
msg = msg & "modem"
End If
If dwFlags And INTERNET_CONNECTION_OFFLINE Then
msg = msg & "offline. "
End If
If dwFlags And INTERNET_CONNECTION_MODEM_BUSY Then
msg = msg & "non-Internetconnection. "
End If
If dwFlags And INTERNET_RAS_INSTALLED Then
msg = msg & "Remote Access Services"
End If
Else
msg = "No conectado a Internet"
End If
GetNetConnectString = "Conexion tipo: " & msg
End Function

