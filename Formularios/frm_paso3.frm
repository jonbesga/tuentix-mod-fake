VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{B927DFF3-9F58-4ADF-9F76-0940FA304A18}#1.0#0"; "WINXPC~1.OCX"
Begin VB.Form frm_paso3 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Asistente de instalación de Tuentix Mod"
   ClientHeight    =   3510
   ClientLeft      =   4485
   ClientTop       =   3780
   ClientWidth     =   6825
   Icon            =   "frm_paso3.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frm_paso3.frx":0A02
   ScaleHeight     =   3510
   ScaleWidth      =   6825
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmd_fin 
      BackColor       =   &H8000000D&
      Caption         =   "Siguiente >"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4920
      TabIndex        =   2
      Top             =   2880
      Width           =   1575
   End
   Begin WinXPC_Engine.WindowsXPC WindowsXPC1 
      Left            =   0
      Top             =   0
      _ExtentX        =   6588
      _ExtentY        =   1085
      ColorScheme     =   1
      Common_Dialog   =   0   'False
   End
   Begin VB.Timer Timer1 
      Interval        =   1500
      Left            =   3720
      Top             =   0
   End
   Begin MSComctlLib.ProgressBar bar_1 
      Height          =   375
      Left            =   960
      TabIndex        =   0
      Top             =   2280
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.Label lbl_1 
      Alignment       =   2  'Center
      BackColor       =   &H80000000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0 %"
      Height          =   255
      Left            =   3120
      TabIndex        =   1
      Top             =   1920
      Width           =   615
   End
End
Attribute VB_Name = "frm_paso3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Private Sub cmd_fin_Click()
frm_paso3.Hide
frm_paso4.Show
End Sub

Private Sub Form_Load()

WindowsXPC1.InitSubClassing

End Sub

Private Sub Timer1_Timer()

Dim X As Long

    For X = 1 To 44
        lbl_1 = X & " %"
        DoEvents
        bar_1.Value = X
        Sleep 75
    Next X

If bar_1.Value = 44 Then
    On Error GoTo Fallo
    X = GetAttr(App.Path & "\xmod.dll")
        
    For X = 44 To 100
        lbl_1 = X & " %"
        DoEvents
        bar_1.Value = X
        Sleep 75
    Next X
        
    If bar_1.Value = 100 Then
        cmd_fin.Enabled = True
        Timer1.Enabled = False
    End If
    
    Exit Sub
End If

Fallo:
noprom = MsgBox("No se encontro un archivo necesario para la instalacion: xmod.dll", 16, "Asistente de instalación de Tuentix Mod")
If noprom = 1 Then
    End
End If

End Sub

