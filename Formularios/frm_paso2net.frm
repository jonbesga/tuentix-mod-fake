VERSION 5.00
Object = "{B927DFF3-9F58-4ADF-9F76-0940FA304A18}#1.0#0"; "WINXPC~1.OCX"
Begin VB.Form frm_paso2net 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Asistente de instalación de Tuentix Mod"
   ClientHeight    =   4005
   ClientLeft      =   4275
   ClientTop       =   3915
   ClientWidth     =   6825
   Icon            =   "frm_paso2net.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frm_paso2net.frx":0A02
   ScaleHeight     =   4005
   ScaleWidth      =   6825
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmd_atras 
      BackColor       =   &H8000000D&
      Caption         =   "< Atras"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   1
      Top             =   3360
      Width           =   1575
   End
   Begin VB.CommandButton cmd_sig2 
      BackColor       =   &H8000000D&
      Caption         =   "Siguiente >"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4920
      TabIndex        =   0
      Top             =   3360
      Width           =   1575
   End
   Begin WinXPC_Engine.WindowsXPC WindowsXPC1 
      Left            =   1560
      Top             =   3240
      _ExtentX        =   6588
      _ExtentY        =   1085
      ColorScheme     =   1
      Common_Dialog   =   0   'False
      CheckControl    =   0   'False
   End
End
Attribute VB_Name = "frm_paso2net"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmd_sig2_Click()
frm_paso2net.Hide
frm_paso3.Show
End Sub

Private Sub Form_Load()

WindowsXPC1.InitSubClassing

End Sub
Private Sub cmd_atras_Click()
frm_paso2net.Hide
frm_paso1.Show
End Sub
